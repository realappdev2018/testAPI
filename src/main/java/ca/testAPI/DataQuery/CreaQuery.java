package ca.testAPI.DataQuery;

import ca.testAPI.Model.XmlObjects.RETS;
import ca.testAPI.Model.XmlObjects.RetsResponse;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

public class CreaQuery {

    private String loginProperty = "";
    private String COOKIES_HEADER = "Set-Cookie";
    private String test = "";
    private CookieManager cookieManager = new CookieManager();
    private String username= "CXLHfDVrziCfvwgCuL8nUahC";
    private String password = "mFqMsCSPdnb5WO1gpEEtDCHH";


    public CreaQuery(String username, String password){
        this.username = username;
        this.password = password;
    }

    public CreaQuery(){}

    public void loginTransaction() throws IOException, NoSuchAlgorithmException {
        URL url = new URL("http://sample.data.crea.ca/Login.svc/Login");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.connect();
        if(urlConnection.getResponseCode() == 401) {
            String authenticationHeader = urlConnection.getHeaderField("WWW-Authenticate");
            System.out.println(authenticationHeader);
            authenticationHeader = authenticationHeader.substring(7);
            String[] vars = authenticationHeader.split(",");
            String realm = vars[0].substring(7, vars[0].length() - 1);
            String nonce = vars[1].substring(8, vars[1].length() - 1);
            String qop = vars[2].substring(6, vars[2].length() - 1);
            String response = "";

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((username + ":" + realm + ":" + password).getBytes());
            String h1 = hash(md);

            md.update("GET:/Login.svc/Login".getBytes());
            String h2 = hash(md);

            md.update((h1 + ":" + nonce + ":::" + qop + ":" + h2).getBytes());
            response = hash(md);
            System.out.println(response);

            URL login = new URL("http://sample.data.crea.ca/login.svc/login");
            HttpURLConnection loginConnection = (HttpURLConnection)login.openConnection();
            loginConnection.setRequestMethod("GET");
            loginProperty = "Digest username=\"" + username + "\", realm=\"" + realm + "\", nonce=\""
                    + nonce + "\", uri=\"/Login.svc/Login\", qop=\"" + qop +
                    "\", cnonce=\"\", response=\"" + response + "\", opaque=\"\"";
            loginConnection.setRequestProperty("Authorization", loginProperty);
            loginConnection.connect();

            System.out.println(loginConnection.getResponseCode());
            System.out.println(loginConnection.getResponseMessage());
            BufferedReader br = new BufferedReader(new InputStreamReader(loginConnection.getInputStream()));
            String s;
            while((s = br.readLine()) != null) {
                System.out.println(s);
            }

            Map<String, List<String>> map = loginConnection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                System.out.println(entry.getKey() +
                        " : " + entry.getValue());
            }

            List<String> cookiesHeader = map.get(COOKIES_HEADER);

            test = cookiesHeader.get(0);
            br.close();
        }
    }

    public void logoutTransaction() throws IOException {

        URL url = new URL("http://sample.data.crea.ca/logout.svc/logout");
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setRequestProperty("Authorization", loginProperty);
        //System.out.println(cookieManager.getCookieStore().get(new URI("http://sample.data.crea.ca")).get(0).toString());
        System.out.println(test);
        urlConnection.addRequestProperty("Cookie", test);
        urlConnection.connect();
        System.out.println(urlConnection.getResponseCode());
        System.out.println(urlConnection.getResponseMessage());
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String s = br.readLine();
        System.out.println(s);
        br.close();
    }

    public String hash(MessageDigest messageDigest){
        byte[] hashInBytes = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public RetsResponse getMasterList() throws Exception {

        URL url = new URL("http://sample.data.crea.ca/Search.svc/Search?Format=Standard-XML&SearchType=Property&Class=Property&QueryType=DMQL2&Culture=en-CA&Query=(ID=*)&Count=1");
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setRequestProperty("Authorization", loginProperty);
        urlConnection.addRequestProperty("Cookie", test);
        urlConnection.connect();
        System.out.println(urlConnection.getResponseCode());
        System.out.println(urlConnection.getResponseMessage());
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String s;
        String file = "";
        while((s = br.readLine()) != null){
            file += s;
        }

        Serializer serializer = new Persister();
        RETS rets = serializer.read(RETS.class, file, false);
        return rets.getRetsResponse();
    }

    public RetsResponse queryIndividualProperty(int propertyID) throws Exception {

        URL url = new URL("http://sample.data.crea.ca/Search.svc/Search?Format=Standard-XML&SearchType=Property&Class=Property&QueryType=DMQL2&Culture=en-CA&Query=(ID=" + propertyID + ")&Count=1");
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setRequestProperty("Authorization", loginProperty);
        urlConnection.addRequestProperty("Cookie", test);
        urlConnection.connect();
        System.out.println(urlConnection.getResponseCode());
        System.out.println(urlConnection.getResponseMessage());
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String s;
        String file = "";
        while((s = br.readLine()) != null){
            file += s;
        }


        br.close();

        Serializer serializer = new Persister();
        RETS rets = serializer.read(RETS.class, file, false);
        return rets.getRetsResponse();
    }
}
