package ca.testAPI.Controller;

import ca.testAPI.DataQuery.CreaQuery;
import ca.testAPI.Model.Repositories.PropertyDetailsRepository;
import ca.testAPI.Model.XmlObjects.Property;
import ca.testAPI.Model.XmlObjects.PropertyDetails;
import ca.testAPI.Model.XmlObjects.RETS;
import ca.testAPI.Model.XmlObjects.RetsResponse;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private PropertyDetailsRepository propertyDetailsRepository;

    @GetMapping(path = "/hello")
    public String hello(){
        return "Hello World!";
    }

    @GetMapping(path = "/test")
    public List<PropertyDetails> test(){
        Serializer serializer = new Persister();
        File file = new File("src/main/resources/Search.xml");
        try {
            RETS rets = serializer.read(RETS.class, file, false);
            propertyDetailsRepository.save(rets.getPropertyDetails());
            List<PropertyDetails> temp =  propertyDetailsRepository.findAllByPropertyID(rets.getPropertyDetails().getPropertyID());
            return temp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(path = "/master-list")
    public List<Property> masterList() throws Exception{
        CreaQuery query = new CreaQuery();
        query.loginTransaction();
        RetsResponse response = query.getMasterList();
        List<Property> temp = response.getPropertyList().subList(0, 100);
        for(Property property : temp){
            RetsResponse propertyQueryResponse = query.queryIndividualProperty(property.getPropertyID());
            System.out.println(propertyQueryResponse.getPropertyDetails().getPropertyID());
            PropertyDetails databaseProperty = propertyDetailsRepository.findByPropertyID(property.getPropertyID());
            if(databaseProperty == null){
                propertyDetailsRepository.save(propertyQueryResponse.getPropertyDetails());
            }
        }
        query.logoutTransaction();
        return response.getPropertyList().subList(0, 10);
    }

    @GetMapping(path = "/get-property/{id}")
    public PropertyDetails getPropertyByID(@PathVariable("id") int id) {
        PropertyDetails propertyDetails = propertyDetailsRepository.findByPropertyID(id);
        System.out.println(propertyDetails.getPropertyID());
        return propertyDetails;
    }
}
