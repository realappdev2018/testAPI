package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import javax.persistence.*;

@Root (name = "Website")
@Entity
@Table (name = "website")
public class Website {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Attribute (name = "ContactType")
    private String contactType;

    @Attribute (name = "WebsiteType")
    private String websiteType;

    @Text
    private String url;
}
