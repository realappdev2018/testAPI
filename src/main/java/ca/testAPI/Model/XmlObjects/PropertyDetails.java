package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "property_details")
@Root (name = "PropertyDetails")
public class PropertyDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Attribute (name = "ID")
    @Column (nullable = false)
    private int propertyID;

    @Attribute (name = "LastUpdated")
    @Column (nullable = false)
    private String updateDate;

    @Element (name = "ListingID", required = false)
    @Column (nullable = false)
    private String listingID;

    @ElementList(name = "AgentDetails", inline = true, required = false)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "agents_id")
    private List<AgentDetails> agentDetailsList;

    @Element(name = "Board", required = false)
    @Column
    private int board;

    @Element(name = "Business", required = false)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "business_id")
    private Business business;

    @Element (name = "Building", required = false)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "building_id")
    private Building building;

    @Element (name = "Land", required = false)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "land_id")
    private Land land;

    @Element (name = "Address", required = false)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @Element (name = "AmmenitiesNearBy", required = false)
    @Column
    private String amenities;

    @Element (name = "CommunityFeatures", required = false)
    @Column
    private String communityFeatures;

    @Element (name = "Features", required = false)
    @Column
    private String features;

    @Element (name = "ListingContractDate", required = false)
    @Column
    private String listingContractDate;

    @Element (name = "LocationDescription", required = false)
    @Column(length = 1000)
    private String locationDescription;

    @Element (name = "ManagementCompany", required = false)
    @Column
    private String managementCompany;

    @Element (name = "MunicipalId", required = false)
    @Column
    private String municipalID;

    @Element (name = "OwnershipType", required = false)
    @Column
    private String ownershipType;

    @Element (name = "ParkingSpaces", required = false)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parking_space_id")
    private ParkingSpace parkingSpace;

    @Element (name = "PoolType", required = false)
    @Column
    private String poolType;

    @Element (name = "Price", required = false)
    @Column
    private double price;

    @Element (name = "PropertyType", required = false)
    @Column
    private String propertyType;

    @Element (name = "PublicRemarks", required = false)
    @Column (length = 4000)
    private String publicRemarks;

    @Element (name = "TransactionType", required = false)
    @Column
    private String transactionType;

    @Element (name = "UtilitiesAvailable", required = false)
    @Column
    private String utilitiesAvailable;

    @Element (name = "ViewType", required = false)
    @Column
    private String viewType;

    public PropertyDetails(){}

    public int getPropertyID() {
        return propertyID;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public String getListingID() {
        return listingID;
    }

    public List<AgentDetails> getAgentDetailsList() {
        return agentDetailsList;
    }

    public int getBoard() {
        return board;
    }

    public Business getBusiness() {
        return business;
    }

    public Building getBuilding() {
        return building;
    }

    public Land getLand() {
        return land;
    }

    public Address getAddress() {
        return address;
    }

    public String getAmenities() {
        return amenities;
    }

    public String getCommunityFeatures() {
        return communityFeatures;
    }

    public String getFeatures() {
        return features;
    }

    public String getListingContractDate() {
        return listingContractDate;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public String getManagementCompany() {
        return managementCompany;
    }

    public String getMunicipalID() {
        return municipalID;
    }

    public String getOwnershipType() {
        return ownershipType;
    }

    public ParkingSpace getParkingSpace() {
        return parkingSpace;
    }

    public String getPoolType() {
        return poolType;
    }

    public double getPrice() {
        return price;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getPublicRemarks() {
        return publicRemarks;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getUtilitiesAvailable() {
        return utilitiesAvailable;
    }

    public String getViewType() {
        return viewType;
    }
}
