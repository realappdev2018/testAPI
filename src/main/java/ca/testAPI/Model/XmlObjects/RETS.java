package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "RETS")
public class RETS {

    @Attribute(name = "ReplyCode")
    private int replyCode;

    @Attribute(name = "ReplyText")
    private String replyText;

    @Element(name = "RETS-RESPONSE")
    private RetsResponse response;

    private PropertyDetails propertyDetails;

    public RETS(@Attribute(name = "ReplyCode") int replyCode, @Attribute(name = "ReplyText") String replyText, @Element(name = "RETS-RESPONSE") RetsResponse response){
        this.replyCode = replyCode;
        this.replyText = replyText;
        this.response = response;
        this.propertyDetails = response.getPropertyDetails();
    }

    public int getReplyCode(){
        return replyCode;
    }

    public String getReplyText(){
        return replyText;
    }

    public RetsResponse getRetsResponse(){
        return response;
    }

    public PropertyDetails getPropertyDetails() { return propertyDetails; }
}
