package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import javax.persistence.*;

@Root(name = "Phone")
@Entity
@Table(name = "phone")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Attribute(name = "ContactType", required = false)
    private String contactType;

    @Attribute(name = "PhoneType", required = false)
    private String phoneType;

    @Text(required = false)
    private String phoneNumber;

    public String getContactType() {
        return contactType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneType(){
        return phoneType;
    }
}
