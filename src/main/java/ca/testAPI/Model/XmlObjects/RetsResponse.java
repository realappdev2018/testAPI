package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


@Root(name = "RETS-RESPONSE")
public class RetsResponse {

    @Element(name = "PropertyDetails", required = false)
    private PropertyDetails propertyDetails;

    @ElementList(name = "Property", required = false, inline = true)
    private List<Property> propertyList;

    public PropertyDetails getPropertyDetails() {
        return propertyDetails;
    }

    public List<Property> getPropertyList(){
        return propertyList;
    }
}
