package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "Property")
public class Property {

    @Attribute(name = "ID")
    private int propertyID;

    @Attribute(name = "LastUpdated")
    private String lastUpdated;

    public int getPropertyID() {
        return propertyID;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }
}
