package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import javax.persistence.*;
import java.util.List;

@Root(name = "Building")
@Entity
@Table(name = "building")
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Element(name = "BathroomTotal", required = false)
    private double bathroomsCount;

    @Element(name = "BedroomsTotal", required = false)
    private int bedroomsCount;

    @Element(name = "BedroomsAboveGround", required = false)
    private int bedroomsAboveGroundCount;

    @Element(name = "BedroomsBelowGround", required = false)
    private int bedroomsBelowGroundCount;

    @Element(name = "Appliances", required = false)
    private String appliances;

    @Element(name = "BasementDevelopment", required = false)
    private String basementDevelopment;

    @Element(name = "BasementType", required = false)
    private String basementType;

    @Element(name = "ConstructionStyleAttachment", required = false)
    private String constructionStyleAttachment;

    @Element(name = "CoolingType", required = false)
    private String coolingType;

    @Element(name = "ExteriorFinish", required = false)
    private String exteriorFinish;

    @Element(name = "FireplacePresent", required = false)
    private String fireplacePresent;

    @Element(name = "FlooringType", required = false)
    private String flooringType;

    @Element(name = "FoundationType", required = false)
    private String foundationType;

    @Element(name = "HalfBathTotal", required = false)
    private int halfBathsCount;

    @ElementList(name = "Rooms", required = false)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rooms_id")
    private List<Room> rooms;

    @Element(name = "StoriesTotal", required = false)
    private double storiesCount;

    @Element(name = "TotalFinishedArea", required = false)
    private String totalFinishedAreal;

    @Element(name = "Type", required = false)
    private String buildingType;

    @Element(name = "UtilityWater", required = false)
    private String utilityWater;

    public double getBathroomsCount() {
        return bathroomsCount;
    }

    public int getBedroomsCount() {
        return bedroomsCount;
    }

    public int getBedroomsAboveGroundCount() {
        return bedroomsAboveGroundCount;
    }

    public int getBedroomsBelowGroundCount() {
        return bedroomsBelowGroundCount;
    }

    public String getAppliances() {
        return appliances;
    }

    public String getBasementDevelopment() {
        return basementDevelopment;
    }

    public String getBasementType() {
        return basementType;
    }

    public String getConstructionStyleAttachment() {
        return constructionStyleAttachment;
    }

    public String getCoolingType() {
        return coolingType;
    }

    public String getExteriorFinish() {
        return exteriorFinish;
    }

    public String getFireplacePresent() {
        return fireplacePresent;
    }

    public String getFlooringType() {
        return flooringType;
    }

    public String getFoundationType() {
        return foundationType;
    }

    public int getHalfBathsCount() {
        return halfBathsCount;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public double getStoriesCount() {
        return storiesCount;
    }

    public String getTotalFinishedAreal() {
        return totalFinishedAreal;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public String getUtilityWater() {
        return utilityWater;
    }
}
