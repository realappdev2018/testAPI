package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javax.persistence.*;

@Root (name = "Business")
@Entity
@Table (name = "business")
public class Business {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long id;

    @Element(name = "Franchise", required = false)
    private String franchise;

    @Element(name = "Name")
    private String name;

    public String getFranchise() {
        return franchise;
    }

    public String getName() {
        return name;
    }
}
