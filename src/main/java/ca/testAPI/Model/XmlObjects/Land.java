package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javax.persistence.*;

@Root(name = "Land")
@Entity
@Table (name = "land")
public class Land {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Element (name = "SizeTotal", required = false)
    private String sizeTotal;

    @Element (name = "SizeTotalText", required = false)
    private String sizeTotalText;

    @Element (name = "SizeFrontage", required = false)
    private String sizeFrontage;

    @Element (name = "SizeDepth", required = false)
    private String sizeDepth;

    @Element (name = "Acreage", required = false)
    private boolean acreage;

    @Element (name = "Amenities", required = false)
    private String amenities;

    @Element (name = "Sewer", required = false)
    private String sewerType;

    public String getSizeTotal() {
        return sizeTotal;
    }

    public String getSizeTotalText() {
        return sizeTotalText;
    }

    public String getSizeFrontage() {
        return sizeFrontage;
    }

    public String getSizeDepth() {
        return sizeDepth;
    }

    public boolean isAcreage() {
        return acreage;
    }

    public String getAmenities() {
        return amenities;
    }

    public String getSewerType() {
        return sewerType;
    }
}
