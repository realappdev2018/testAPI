package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root (name = "Office")
public class Office {

    @Attribute (name = "ID")
    private int officeID;

    @Element(name = "Name")
    private String name;

    @Element (name = "Address", required = false)
    private Address address;

    @ElementList (name = "Phones", required = false)
    private List<Phone> phoneList;

    @ElementList (name = "Websites", required = false)
    private List<Website> websiteList;

    @Element (name = "OrganizationType", required = false)
    private String organizationType;

    public int getOfficeID() {
        return officeID;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public List<Website> getWebsiteList() {
        return websiteList;
    }

    public String getOrganizationType() {
        return organizationType;
    }
}
