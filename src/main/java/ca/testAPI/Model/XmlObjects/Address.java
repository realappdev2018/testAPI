package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javax.persistence.*;

@Root(name = "Address")
@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Element (name = "StreetAddress", required = false)
    private String streetAddress;

    @Element (name = "AddressLine1", required = false)
    private String address1;

    @Element (name = "City", required = false)
    private String city;

    @Element (name = "Province", required = false)
    private String province;

    @Element (name = "PostalCode", required = false)
    private String postalCode;

    @Element (name = "Country", required = false)
    private String country;

    @Element (name = "CommunityName", required = false)
    private String communityName;

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getAddress1() {
        return address1;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCommunityName() {
        return communityName;
    }
}
