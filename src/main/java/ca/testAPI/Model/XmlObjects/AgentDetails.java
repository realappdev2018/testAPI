package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import javax.persistence.*;
import java.util.List;

@Root(name = "AgentDetails")
@Entity
@Table(name = "agent_details")
public class AgentDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Attribute(name = "ID")
    private int agentID;

    @Element(name = "Name")
    private String agentName;

    @ElementList(name = "Phones", required = false)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "phone_list_id")
    private List<Phone> phoneList;

    @ElementList(name = "Websites", required = false)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "website_list_id")
    private List<Website> websiteList;

    /*public List<Phone> getPhoneList() {
        return phoneList;
    }*/

    /*public List<Website> getWebsiteList() {
        return websiteList;
    }*/

    public String getAgentName(){
        return agentName;
    }

    public int getAgentID(){
        return  agentID;
    }

}
