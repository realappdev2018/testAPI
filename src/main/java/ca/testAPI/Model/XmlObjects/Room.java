package ca.testAPI.Model.XmlObjects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javax.persistence.*;

@Root (name = "Room")
@Entity
@Table(name = "room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Element (name = "Type")
    private String roomType;

    @Element (name = "Level", required = false)
    private String level;

    @Element (name = "Dimension", required = false)
    private String dimension;

    public String getRoomType() {
        return roomType;
    }


    public String getLevel() {
        return level;
    }

    public Room(){}

    public Room(@Element(name = "Type")String roomType, @Element(name = "Level")String level, @Element(name = "Dimension")String dimension){
        this.roomType = roomType;
        this.level = level;
        this.dimension = dimension;
    }
}
