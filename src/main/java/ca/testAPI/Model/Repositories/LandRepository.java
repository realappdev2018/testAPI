package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Land;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LandRepository extends JpaRepository<Land, Long> {
}
