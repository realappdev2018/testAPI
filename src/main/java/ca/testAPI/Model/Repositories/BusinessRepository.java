package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Business;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessRepository extends JpaRepository<Business, Long> {
}
