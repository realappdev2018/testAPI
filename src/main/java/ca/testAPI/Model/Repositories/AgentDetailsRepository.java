package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.AgentDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgentDetailsRepository extends JpaRepository<AgentDetails, Long> {
}
