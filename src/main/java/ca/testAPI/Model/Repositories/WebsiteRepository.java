package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Website;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebsiteRepository extends JpaRepository<Website, Long> {
}
