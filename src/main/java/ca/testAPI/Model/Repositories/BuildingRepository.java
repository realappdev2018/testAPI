package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Building;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long> {

}
