package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.PropertyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyDetailsRepository extends JpaRepository<PropertyDetails, Long> {
    List<PropertyDetails> findAllByPropertyID(@Param("propertyID") int propertyID);
    PropertyDetails findByPropertyID(@Param("propertyID") int propertyID);
    PropertyDetails save(PropertyDetails propertyDetails);
}
