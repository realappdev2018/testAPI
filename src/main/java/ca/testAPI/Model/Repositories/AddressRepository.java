package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
