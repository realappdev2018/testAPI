package ca.testAPI.Model.Repositories;

import ca.testAPI.Model.XmlObjects.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}
